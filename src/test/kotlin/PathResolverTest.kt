import org.example.PathResolver
import org.junit.jupiter.api.Test
import java.nio.file.Path
import kotlin.test.assertEquals

class PathResolverTest {

  private fun resolve(start: String, vararg path: String): String {
    return Path.of(start, *path).toString()
  }

  @Test
  fun `simple path`() {
    assertEquals(resolve("a"), PathResolver.resolve("a"))
  }

  @Test
  fun `abcd path`() {
    assertEquals(resolve("a", "b", "c", "d"), PathResolver.resolve("a", "b", "c", "d"))
  }

  @Test
  fun `aaaa path`() {
    assertEquals(resolve("a", "a", "a", "a"), PathResolver.resolve("a", "a", "a", "a"))
  }
}